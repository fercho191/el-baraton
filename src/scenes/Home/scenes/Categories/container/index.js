import { connect } from 'react-redux';
import Categories from "../index";


const mapStateToProps = (state) => {
  return {
    homeReducer : state.homeReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

const CategoriesContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories);

export default CategoriesContainer;