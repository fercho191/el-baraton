import React, { Component } from 'react';
import {Button, Col} from "antd";
import {Link} from "react-router-dom";
import './styles/Category.css';

class Category extends Component {
  constructor(){
    super();

    this.onCategoryClick = this.onCategoryClick.bind(this);
  }

  onCategoryClick(){
    this.props.selectCategory(this.props.item)
  }

  render(){
    const {item} = this.props;

    return(
      <Col span={12} className="category">
        <Link to={`/categories/${item.id}/products`}>
          <Button
            className="category-button"
            type="primary"
            onClick={this.onCategoryClick}>
            {item.name}
          </Button>
        </Link>
      </Col>
    )
  }
}

export default Category