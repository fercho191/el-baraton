import React, { Component } from 'react';
import {Breadcrumb, Layout, Row} from "antd";
import {Link} from "react-router-dom";
import './styles/Categories.css';
import CategoryContainer from "./components/Category/container";

const { Content } = Layout;

class Categories extends Component{

  render(){
    const { categories } = this.props.homeReducer;

    return(
      <Content className="categories">
        <Breadcrumb className="breadcrumb">
          <Breadcrumb.Item><Link to='/categories'>Categories</Link></Breadcrumb.Item>
        </Breadcrumb>
        <div className="page-content">
          <Row type="flex" justify="center">
            <h2>Selecciona una de nuestras categorias</h2>
          </Row>
          <Row type="flex">
          {
            categories && categories.map(item => {
              return (
                <CategoryContainer
                  key={`category${item.id}`}
                  item={item}/>
              )
            })
          }
          </Row>
        </div>
      </Content>
    )
  }
}

export default Categories