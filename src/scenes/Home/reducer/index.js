import * as types from '../actions/types';

export const initialState = {
  categories: null,
  selectedCategory: null,
  products: null,
  filteredProducts: null,
  selectedFilters: {
    available: true,
    priceRange:{
      min: null,
      max: null
    },
    subCategoryId: null
  }
};

export const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CATEGORIES_RECEIVE:
      return {
        ...state,
        categories: action.categories
      };
    case types.CATEGORY_SELECT:
      return {
        ...state,
        selectedCategory: action.category
      };
    case types.AVAILABLE_FILTER_TOGGLE:
      return {
        ...state,
        selectedFilters: {
          ...state.selectedFilters,
          available: !state.selectedFilters.available
        }
      };
    case types.PRICE_RANGE_FILTER_SET:
      return {
        ...state,
        selectedFilters: {
          ...state.selectedFilters,
          priceRange: {
            ...state.selectedFilters.priceRange,
            min: action.min,
            max: action.max
          }
        }
      };
    case types.SUB_CATEGORY_FILTER_SET:
      return {
        ...state,
        selectedFilters: {
          ...state.selectedFilters,
          subCategoryId: action.subCategoryId
        }
      };
    case types.PRODUCTS_RECEIVE:
      return {
        ...state,
        products: action.products,
        filteredProducts: action.products
      };
    default:
      return state;
  }
};