import React, { Component } from 'react';
import { Layout } from 'antd';
import {Route} from "react-router";
import HomeSider from "../../components/HomeSider";
import Cart from "./scenes/Cart";
import CategoriesContainer from "./scenes/Categories/container";
import ProductListContainer from "./scenes/ProductList/container";
import categories from 'data/categories';
import './styles/Home.css';

const { Header, Footer } = Layout;

class Home extends Component {

  componentDidMount(){
    this.props.receiveCategories(categories);
  }

  render(){
    return(
      <Layout className="home">
        <HomeSider/>
        <Layout>
          <Header className="header"/>
          <div>
            <Route exact path="/categories" component={CategoriesContainer}/>
            <Route path="/shopping-cart" component={Cart}/>
            <Route path="/categories/:categoryId/products" component={ProductListContainer}/>
          </div>
          <Footer className="footer">
            El baraton ©2018 Created by Fercho
          </Footer>
        </Layout>
      </Layout>
    )
  }
}

export default Home